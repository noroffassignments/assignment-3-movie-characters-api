# Assignment 3 - Movie Characters API
This repository contains the code for the Assignment 3 - Movie Characters API project. The API allows you to retrieve, create, update and delete information about movie characters.

## Getting Started
These instructions will help you get a copy of the project up and running on your local machine for development and testing purposes.

## Prerequisites
The following tools and software are required to run the API:

- .NET Core 5.0 or higher
- Visual Studio 2019 or higher (optional)
- Postman or a similar API client for testing purposes
- SQL Server Management Studio

## Installing
Follow these steps to install the project on your local machine:

1. Clone the repository
2. Open the solution in Visual Studio.
3. Restore the NuGet packages by right-clicking the solution in the Solution Explorer and selecting "Restore NuGet Packages".
4. Build the solution by right-clicking the solution in the Solution Explorer and selecting "Build Solution".
5. In the Package Manager Console use "update-database".
5. Start the API by running the project "assignment_3_movie_character_api".

The API should now be up and running. You can test it using Postman or a similar API client.

## API Documentation
The API has the following endpoints:

- GET /api/characters: Retrieve a list of all movie characters.
- GET /api/characters/{id}: Retrieve a single movie character by its id.
- POST /api/characters: Create a new movie character.
- PUT /api/characters/{id}: Update an existing movie character.
- DELETE /api/characters/{id}: Delete an existing movie character.

- GET /api/movies: Retrieve a list of all movies.
- GET /api/movies/{id}: Retrieve a single movie by its id.
- POST /api/movie: Create a new movie.
- PUT /api/movie/{id}: Update an existing movie.
- DELETE /api/movie/{id}: Delete an existing movie.
- PUT ​/api​/movies​/{id}​/Characters Update characters in movies.
- GET ​/api​/movies​/AllCharactersInMovie Get all characters in movies.

- GET /api/franchises: Retrieve a list of all franchises.
- GET /api/fracnhises/{id}: Retrieve a single franchise by its id.
- POST /api/franchises: Create a new franchise.
- PUT /api/franchises/{id}: Update an existing franchise.
- DELETE /api/franchises/{id}: Delete an existing franchise.
- PUT ​/api​/franchises/{id}​/movies Update movies in franchises.
- GET ​/api​/frnachises/AllCharactersInFranchise Get all characters in a frachise.

## Contributing
- Niels van Mierlo https://gitlab.com/NVM238
- Niek Veenstra https://gitlab.com/NiekVeenstra


## License
This project is licensed under the MIT License.