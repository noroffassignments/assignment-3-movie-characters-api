﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using assignment_3_movie_character_api.Data;
using assignment_3_movie_character_api.Models;
using AutoMapper;
using assignment_3_movie_character_api.DataTransferObjects;
using assignment_3_movie_character_api.Services;

namespace assignment_3_movie_character_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class CharactersController : ControllerBase
    {
        private readonly CharactersDbContext _context;
        private readonly IMapper _mapper;
        private readonly ICharacterServices _characterServices;

        public CharactersController(CharactersDbContext context, IMapper mapper, ICharacterServices characterServices)
        {
            _context = context;
            _mapper = mapper;
            _characterServices = characterServices;
        }

    /// <summary>
    /// Get all characters
    /// </summary>
        // GET: api/Characters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTOReadCharacter>>> GetCharacters()
        {
            /*var CharacterModel = await _context.Characters.ToListAsync();
            var CharacterDto = _mapper.Map<List<DTOReadCharacter>>(CharacterModel);
            return CharacterDto;*/

            return _mapper.Map<List<DTOReadCharacter>>(await _characterServices.GetAllCharactersAsync());
        }
        /// <summary>
        /// Get specific character by id
        /// </summary>
        // GET: api/Characters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTOReadCharacter>> GetCharacter(int id)
        {
            var characterModel = await _characterServices.GetSpecificCharacterAsync(id);

            if (characterModel == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTOReadCharacter>(characterModel);
        }

        /// <summary>
        /// Update specific character by id
        /// </summary>
        // PUT: api/Characters/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, DTOEditCharacter dtoCharacter)
        {
            if (id != dtoCharacter.Id)
            {
                return BadRequest();
            }

            if (!_characterServices.CharacterExists(id))
            {
                return NotFound();
            }

            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);
            await _characterServices.UpdateCharacterAsync(domainCharacter);
            return NoContent();

        }
        /// <summary>
        /// Add character
        /// </summary>
        // POST: api/Characters
        [HttpPost]
        public async Task<ActionResult<DTOReadCharacter>> PostCharacter(DTOCreateCharacter characterDto)
        {
            Character domainCharacter = _mapper.Map<Character>(characterDto);
            domainCharacter = await _characterServices.AddCharacterAsync(domainCharacter);

            return CreatedAtAction("GetCharacter", new { id = domainCharacter.Id },
                _mapper.Map<DTOReadCharacter>(domainCharacter));
        }
        /// <summary>
        /// Delete character by id
        /// </summary>
        // DELETE: api/Characters/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!_characterServices.CharacterExists(id))
            {
                return NotFound();
            }

            await _characterServices.DeleteCharacterAsync(id);
            return NoContent();
        }
    }
}
