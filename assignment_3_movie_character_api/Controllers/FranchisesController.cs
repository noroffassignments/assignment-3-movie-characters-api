﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using assignment_3_movie_character_api.Data;
using assignment_3_movie_character_api.Models;
using assignment_3_movie_character_api.DataTransferObjects;
using assignment_3_movie_character_api.Services;
using AutoMapper;

namespace assignment_3_movie_character_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly CharactersDbContext _context;
        private readonly IMapper _mapper;
        private readonly IFranchiseServices _franchiseServices;

        public FranchisesController(CharactersDbContext context, IMapper mapper, IFranchiseServices franchiseServices)
        {
            _context = context;
            _mapper = mapper;
            _franchiseServices = franchiseServices;
        }
        /// <summary>
        /// Get all franchices
        /// </summary>
        // GET: api/Franchise
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTOReadFranchise>>> GetFranchises()
        {
            return _mapper.Map<List<DTOReadFranchise>>(await _franchiseServices.GetAllFranchisesAsync());
        }
        /// <summary>
        /// Get specific franchice by id
        /// </summary>
        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTOReadFranchise>> GetFranchise(int id)
        {
            var franchiseModel = await _franchiseServices.GetSpecificFranchiseAsync(id);

            if (franchiseModel == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTOReadFranchise>(franchiseModel);
        }
        /// <summary>
        /// Update franchice by id
        /// </summary>
        // PUT: api/Franchise/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, DTOEditFranchise dtoFranchise)
        {
            if (id != dtoFranchise.Id)
            {
                return BadRequest();
            }

            if (!_franchiseServices.FranchiseExists(id))
            {
                return NotFound();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);
            await _franchiseServices.UpdateFranchiseAsync(domainFranchise);
            return NoContent();

        }
        /// <summary>
        /// Add franchice
        /// </summary>
        // POST: api/Franchise
        [HttpPost]
        public async Task<ActionResult<DTOReadFranchise>> PostFranchise(DTOCreateFranchise franchiseDto)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(franchiseDto);
            domainFranchise = await _franchiseServices.AddFranchiseAsync(domainFranchise);

            return CreatedAtAction("GetFranchise", new { id = domainFranchise.Id },
                _mapper.Map<DTOReadFranchise>(domainFranchise));
        }
        /// <summary>
        /// Delete franchice
        /// </summary>
        // DELETE: api/Franchise/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_franchiseServices.FranchiseExists(id))
            {
                return NotFound();
            }

            await _franchiseServices.DeleteFranchiseAsync(id);
            return NoContent();
        }
        /// <summary>
        /// Update movies in franchise
        /// </summary>
        [HttpPut("{id}/Movies")]
        public async Task<IActionResult> UpdateMovieFranchise(int id, List<int> movies)
        {
            return await _franchiseServices.UpdateMovieFranchise(id, movies);
        }
        /// <summary>
        /// Show all movies in franchice
        /// </summary>
        [HttpGet("AllMoviesInFranchise")]
        public async Task<ActionResult<List<DTOReadMovie>>> GetAllMoviesInFranchise(int franchiseId)
        {
            return await _franchiseServices.GetAllMoviesInFranchise(franchiseId);
        }
        /// <summary>
        /// Show all characters in franchise
        /// </summary>
        [HttpGet("AllCharactersInFranchise")]
        public async Task<ActionResult<List<DTOReadCharacter>>> GetAllCharactersInFranchise(int franchiseId)
        {
            return await _franchiseServices.GetAllCharactersInFranchise(franchiseId);
        }
    }
}
