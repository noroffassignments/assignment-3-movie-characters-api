﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using assignment_3_movie_character_api.Data;
using assignment_3_movie_character_api.Models;
using assignment_3_movie_character_api.DataTransferObjects;
using assignment_3_movie_character_api.Services;
using AutoMapper;

namespace assignment_3_movie_character_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly CharactersDbContext _context;
        private readonly IMapper _mapper;
        private IMovieServices _movieServices;

        public MoviesController(CharactersDbContext context, IMapper mapper, IMovieServices movieServices)
        {
            _context = context;
            _mapper = mapper;
            _movieServices = movieServices;
        }
        /// <summary>
        /// Get all movies
        /// </summary>
        // GET: api/Movies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTOReadMovie>>> GetMovies()
        {
            return _mapper.Map<List<DTOReadMovie>>(await _movieServices.GetAllMoviesAsync());
        }
        /// <summary>
        /// Get movie by id
        /// </summary>
        // GET: api/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTOReadMovie>> GetMovie(int id)
        {
            var movieModel = await _movieServices.GetSpecificMovieAsync(id);

            if (movieModel == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTOReadMovie>(movieModel);
        }
        /// <summary>
        /// Update movie by id
        /// </summary>
        // PUT: api/Movies/5      
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, DTOEditMovie dtoMovie)
        {
            if (id != dtoMovie.Id)
            {
                return BadRequest();
            }

            if (!_movieServices.MovieExists(id))
            {
                return NotFound();
            }

            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);
            await _movieServices.UpdateMovieAsync(domainMovie);
            return NoContent();

        }
        /// <summary>
        /// Add movie
        /// </summary>
        // POST: api/Movies
        [HttpPost]
        public async Task<ActionResult<DTOReadMovie>> PostMovie(DTOCreateMovie movieDto)
        {
            Movie domainMovie = _mapper.Map<Movie>(movieDto);
            domainMovie = await _movieServices.AddMovieAsync(domainMovie);

            return CreatedAtAction("GetMovie", new { id = domainMovie.Id },
                _mapper.Map<DTOReadMovie>(domainMovie));
        }
        /// <summary>
        /// Delete movie
        /// </summary>
        // DELETE: api/Movie/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieServices.MovieExists(id))
            {
                return NotFound();
            }

            await _movieServices.DeleteMovieAsync(id);
            return NoContent();
        }
        /// <summary>
        /// Update characters in movies
        /// </summary>
        [HttpPut("{id}/Characters")]
        public async Task<IActionResult> UpdateCharacterMovie(int id, List<int> characters)
        {
            return await _movieServices.UpdateCharacterMovie(id, characters);
        }
        /// <summary>
        /// Get all characters in movies
        /// </summary>
        [HttpGet("AllCharactersInMovie")]
        public async Task<List<DTOReadCharacter>> GetAllCharactersInMovie(int movieId)
        {
            return await _movieServices.GetAllCharactersInMovie(movieId);
        }
    }
}
