﻿using assignment_3_movie_character_api.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace assignment_3_movie_character_api.Data
{
    public class CharactersDbContext : DbContext
    {
        public CharactersDbContext(DbContextOptions options) : base(options)
        {
        }

        // DbSet for each of the entities in the model: Movie, Character, and Franchise
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seeds data for the Character entity
            modelBuilder.Entity<Character>().HasData(SeedDataHelper.GetCharacters());

            // Seeds data for the Franchise entity
            modelBuilder.Entity<Franchise>().HasData(SeedDataHelper.GetFranchises());

            // Seeds data for the Movie entity
            modelBuilder.Entity<Movie>().HasData(SeedDataHelper.GetMovies());

            // Creates many-to-many relationship between Movie and Character entities
            modelBuilder.Entity<Movie>()
            .HasMany(p => p.Characters)
            .WithMany(m => m.Movies)
            .UsingEntity<Dictionary<string, object>>(
                "CharacterMovie",
                r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                je =>
                {
                    je.HasKey("MovieId", "CharacterId");
                    je.HasData(
                        SeedDataHelper.GetData()
                    ) ;
                });

        }
        
    }
}
