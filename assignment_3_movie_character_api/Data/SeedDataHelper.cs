﻿using assignment_3_movie_character_api.Models;
using System.Collections.Generic;

namespace assignment_3_movie_character_api.Data
{
    // This class `SeedDataHelper` provides helper methods to initialize data for various models
    public class SeedDataHelper
    {
        // GetCharacters method returns a list of Character objects with predefined data
        public static List<Character> GetCharacters()
        {
            // Create a list of Character objects
            List<Character> characters = new List<Character>()
            {
                new Character() {Id=1, FullName="Niek", Alias="Meister", Gender="Male", PictureURL="https://commons.wikimedia.org/wiki/File:Placeholder_Person.jpg"},
                new Character() {Id=2, FullName="Niels", Alias="Testing", Gender="Male", PictureURL="https://commons.wikimedia.org/wiki/File:Placeholder_Person.jpg"},
                new Character() {Id=3, FullName="Lala", Alias="Loo", Gender="Female", PictureURL="https://commons.wikimedia.org/wiki/File:Placeholder_Person.jpg"},
            };
            return characters;
        }

        // GetFranchises method returns a list of Franchise objects with predefined data
        public static List<Franchise> GetFranchises()
        {
            // Create a list of Franchise objects
            List<Franchise> franchises = new List<Franchise>()
            {
                new Franchise() {Id=1, Name="Disney", Description="Disney dummy text"},
                new Franchise() {Id=2, Name="Netflix", Description="Netflix dummy text"},
                new Franchise() {Id=3, Name="TestingName", Description="Testing dummy text"},
            };
            return franchises;
        }

        // GetMovies method returns a list of Movie objects with predefined data
        public static List<Movie> GetMovies()
        {
            // Create a list of Movie objects
            List<Movie> movies = new List<Movie>()
            {
                new Movie() {Id=1, MovieTitle="Movie1", ReleaseYear=2001, Director="me1", PictureURL="testingPicURL1", TrailerURL="testingTrailerURL1", FranchiseId=1},
                new Movie() {Id=2, MovieTitle="Movie2", ReleaseYear=2002, Director="me2", PictureURL="testingPicURL2", TrailerURL="testingTrailerURL2", FranchiseId=2},
                new Movie() {Id=3, MovieTitle="Movie3", ReleaseYear=2003, Director="me3", PictureURL="testingPicURL3", TrailerURL="testingTrailerURL3", FranchiseId=3},
            };
            return movies;
        }

        // GetData method returns a list of CharacterMovie objects with predefined data
        public static List<CharacterMovie> GetData()
        {
            // Create a list of CharacterMovie objects
            List<CharacterMovie> charachterMovies = new List<CharacterMovie>
            {
                new CharacterMovie { MovieId = 1, CharacterId = 1 },
                new CharacterMovie { MovieId = 1, CharacterId = 2 },
                new CharacterMovie { MovieId = 1, CharacterId = 3 },
                new CharacterMovie { MovieId = 2, CharacterId = 1 },
                new CharacterMovie { MovieId = 2, CharacterId = 2 },
                new CharacterMovie { MovieId = 2, CharacterId = 3 },
                new CharacterMovie { MovieId = 3, CharacterId = 2 },
                new CharacterMovie { MovieId = 3, CharacterId = 3 }
            };
            return charachterMovies;
        }

    }
}
