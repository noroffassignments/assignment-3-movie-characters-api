﻿using System.ComponentModel.DataAnnotations;

namespace assignment_3_movie_character_api.DataTransferObjects
{
    // Defining the DTO class for creating a character
    public class DTOCreateCharacter
    {
        // Set the properties
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string PictureURL { get; set; }
    }
}
