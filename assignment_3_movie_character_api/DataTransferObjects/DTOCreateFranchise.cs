﻿using assignment_3_movie_character_api.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace assignment_3_movie_character_api.DataTransferObjects
{
    // Defining the DTO class for creating a Franchise
    public class DTOCreateFranchise
    {
        // Set the properties
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
