﻿using System.ComponentModel.DataAnnotations;

namespace assignment_3_movie_character_api.DataTransferObjects
{
    // Defining the DTO class for creating a Movie
    public class DTOCreateMovie
    {
        // Set the properties
        public string MovieTitle { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string PictureURL { get; set; }
        public string TrailerURL { get; set; }
        public int FranchiseId { get; set; }
    }
}
