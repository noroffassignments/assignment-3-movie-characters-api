﻿using System.ComponentModel.DataAnnotations;

namespace assignment_3_movie_character_api.DataTransferObjects
{
    // Defining the DTO class for editing a Character
    public class DTOEditCharacter
    {
        // Set the properties
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string PictureURL { get; set; }
    }
}
