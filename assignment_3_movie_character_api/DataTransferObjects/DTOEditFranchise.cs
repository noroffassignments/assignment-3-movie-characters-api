﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace assignment_3_movie_character_api.DataTransferObjects
{
    // Defining the DTO class for editing a Franchise
    public class DTOEditFranchise
    {
        // Set the properties
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
