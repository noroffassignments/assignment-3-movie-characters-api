﻿using System.Collections.Generic;

namespace assignment_3_movie_character_api.DataTransferObjects
{
    // Defining the DTO class for reading a Character
    public class DTOReadCharacter
    {
        // Set the properties
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string PictureURL { get; set; }

        public List<string> Movies { get; set; }

    }
}
