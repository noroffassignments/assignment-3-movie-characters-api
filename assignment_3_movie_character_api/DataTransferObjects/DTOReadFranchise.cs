﻿using System.Collections.Generic;

namespace assignment_3_movie_character_api.DataTransferObjects
{
    // Defining the DTO class for reading a Franchise
    public class DTOReadFranchise
    {
        // Set the properties
        public string Name { get; set; }
        public string Description { get; set; }


        public List<string> Movies { get; set; }
    }
}
