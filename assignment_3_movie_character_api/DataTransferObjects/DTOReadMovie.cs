﻿using System.Collections.Generic;

namespace assignment_3_movie_character_api.DataTransferObjects
{
    // Defining the DTO class for reading a Movie
    public class DTOReadMovie
    {
        // Set the properties
        public string MovieTitle { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string PictureURL { get; set; }
        public string TrailerURL { get; set; }
        public List<string> Characters { get; set; }
    }
}
