﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace assignment_3_movie_character_api.Models
{
    // Defining the Model class for a Character
    public class Character
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string FullName { get; set; }
        [MaxLength(100)]
        public string Alias { get; set; }
        [MaxLength(50)]
        public string Gender { get; set; }
        [MaxLength(300)]
        public string PictureURL { get; set; }
        
        public ICollection<Movie> Movies { get; set; }
    }
}
