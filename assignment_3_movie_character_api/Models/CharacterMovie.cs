﻿using System.ComponentModel.DataAnnotations;

namespace assignment_3_movie_character_api.Models
{
    // Defining the Model class for a CharacterMovie
    public class CharacterMovie
    {
        // Set the properties
        [Required]
        public int CharacterId { get; set; }
        public Character Character { get; set; }

        [Required]
        public int MovieId { get; set; }
        public Movie Movie { get; set; }
    }
}
