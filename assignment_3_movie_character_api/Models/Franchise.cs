﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace assignment_3_movie_character_api.Models
{
    // Defining the Model class for a Franchise
    public class Franchise
    {
        // Set the properties
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        [Required]
        [MaxLength(1000)]
        public string Description { get; set; }


        public ICollection<Movie> Movies { get; set; }

    }
}
