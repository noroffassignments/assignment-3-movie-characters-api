﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace assignment_3_movie_character_api.Models
{
    // Defining the Model class for a Movie
    public class Movie
    {
        // Set the properties
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string MovieTitle { get; set; }
        [Required]
        [MaxLength(4)]
        public int ReleaseYear { get; set; }
        [Required]
        [MaxLength(100)]
        public string Director { get; set; }
        [MaxLength(300)]
        public string PictureURL { get; set; }
        [MaxLength(300)]
        public string TrailerURL { get; set; }


        public ICollection<Character> Characters { get; set; }

        public int FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
    }
}
