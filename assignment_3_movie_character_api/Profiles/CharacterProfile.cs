﻿using assignment_3_movie_character_api.DataTransferObjects;
using assignment_3_movie_character_api.Models;
using AutoMapper;
using System.Linq;

namespace assignment_3_movie_character_api.Profiles
{
    // Class CharacterProfile which inherits from AutoMapper's Profile class
    public class CharacterProfile : Profile
    {
        // Constructor to define the mapping between Character and DTOReadCharacter, DTOCreateCharacter, and DTOEditCharacter
        public CharacterProfile()
        {
            // Mapping Character to DTOReadCharacter using AutoMapper's CreateMap method
            // ForMember method is used to specify a custom mapping for the 'Movies' property
            CreateMap<Character, DTOReadCharacter>().ForMember(dest => dest.Movies, opt => opt.MapFrom(src => src.Id));
            // Mapping DTOCreateCharacter to Character
            CreateMap<DTOCreateCharacter, Character>();
            // Mapping DTOEditCharacter to Character
            CreateMap<DTOEditCharacter, Character>();
        }
    }
}
