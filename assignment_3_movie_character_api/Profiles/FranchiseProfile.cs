﻿using assignment_3_movie_character_api.DataTransferObjects;
using assignment_3_movie_character_api.Models;
using AutoMapper;
using System.Linq;

namespace assignment_3_movie_character_api.Profiles
{
    // Class FranchiseProfile which inherits from AutoMapper's Profile class
    public class FranchiseProfile : Profile
    {
        // Constructor to define the mapping between Franchise and DTOReadFranchise, DTOCreateFranchise, and DTOEditFranchise
        public FranchiseProfile() {
            // Mapping Franchise to DTOReadFranchise using AutoMapper's CreateMap method
            // ForMember method is used to specify a custom mapping for the 'Movies' property
            CreateMap<Franchise, DTOReadFranchise>().ForMember(mdto => mdto.Movies, opt => opt
                    .MapFrom(m => m.Movies.Select(c => c.MovieTitle).ToList())).ReverseMap();
            // Mapping DTOCreateFranchise to Franchise
            CreateMap<DTOCreateFranchise, Franchise>();
            // Mapping DTOEditFranchise to Franchise
            CreateMap<DTOEditFranchise, Franchise>();
        }
    }
}
