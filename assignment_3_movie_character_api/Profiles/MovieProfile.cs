﻿using assignment_3_movie_character_api.DataTransferObjects;
using assignment_3_movie_character_api.Models;
using AutoMapper;
using System.Linq;

namespace assignment_3_movie_character_api.Profiles
{
    // Class MovieProfile which inherits from AutoMapper's Profile class
    public class MovieProfile : Profile
    {
        // Constructor to define the mapping between Movie and DTOReadMovie, DTOCreateMovie, and DTOEditMovie
        public MovieProfile()
        {
            // Mapping Movie to DTOReadMovie using AutoMapper's CreateMap method
            // ForMember method is used to specify a custom mapping for the 'Character' property
            CreateMap<Movie, DTOReadMovie>().ForMember(mdto => mdto.Characters, opt => opt
                    .MapFrom(m => m.Characters.Select(c => c.FullName).ToList())).ReverseMap();
            // Mapping DTOCreateMovie to Movie
            CreateMap<DTOCreateMovie, Movie>();
            // Mapping DTOEditMovie to Movie
            CreateMap<DTOEditMovie, Movie>();
        }
    }
}
