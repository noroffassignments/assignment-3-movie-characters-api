﻿using assignment_3_movie_character_api.Data;
using assignment_3_movie_character_api.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_3_movie_character_api.Services
{
    public class CharacterServices : ICharacterServices
    {
        private readonly CharactersDbContext _context;

        // Dependency Injection of CharactersDbContext
        public CharacterServices(CharactersDbContext context)
        {
            _context = context;
        }

        // Returns all the characters from the database
        public async Task<IEnumerable<Character>> GetAllCharactersAsync()
        {
            return await _context.Characters.ToListAsync();
        }

        // Returns a specific character from the database
        public async Task<Character> GetSpecificCharacterAsync(int id)
        {
            return await _context.Characters.FirstOrDefaultAsync(m => m.Id == id);
        }

        // Updates the character in the database
        public async Task UpdateCharacterAsync(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        // Adds a character to the database
        public async Task<Character> AddCharacterAsync(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }

        // Deletes a character from the database
        public async Task DeleteCharacterAsync(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
        }

        // Checks if the character exists in the database
        public bool CharacterExists(int id)
        {
            return _context.Characters.Any(m => m.Id == id);
        }
        
    }
}
