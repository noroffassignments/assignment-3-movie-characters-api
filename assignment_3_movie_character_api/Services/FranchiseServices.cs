﻿using assignment_3_movie_character_api.Data;
using assignment_3_movie_character_api.DataTransferObjects;
using assignment_3_movie_character_api.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_3_movie_character_api.Services
{
    public class FranchiseServices : IFranchiseServices
    {
        // Member variables to store DbContext and IMapper instances
        private readonly CharactersDbContext _context;
        private readonly IMapper mapper;
        private MovieServices movieServices;

        // Constructor to initialize member variables using dependency injection
        public FranchiseServices(CharactersDbContext context, IMapper mapper, [FromServices] MovieServices mServices)
        {
            _context = context;
            this.mapper = mapper;
            movieServices = mServices;
        }

        // Method to get all franchises from the database
        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            // Returns the list of franchises with related movies
            return await _context.Franchises.Include(m => m.Movies).ToListAsync();
        }

        // Method to get a specific franchise by id from the database
        public async Task<Franchise> GetSpecificFranchiseAsync(int id)
        {
            // Returns the franchise with related movies matching the provided id
            return await _context.Franchises.Include(m => m.Movies).FirstOrDefaultAsync(m => m.Id == id);
        }

        // Method to update an existing franchise in the database
        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            //Updates the franchise in the database and saves the changes
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        // Method to add a new franchise to the database
        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            // Adds the franchise to the database and saves the changes
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        // Method to delete a franchise from the database
        public async Task DeleteFranchiseAsync(int id)
        {
            // Finds the franchise with the matching id and removes it from the database
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        // Method to check if a franchise exists in the database
        public bool FranchiseExists(int id)
        {
            // Returns true if a franchise with the provided id exists in the database
            return _context.Franchises.Any(m => m.Id == id);
        }

        // Method to update the movies of a franchise in the database
        public async Task<IActionResult> UpdateMovieFranchise(int id, List<int> movies)
        {
            // Checks if the franchise exists
            if (!FranchiseExists(id))
            {
                // Returns a "Not Found" result if the franchise does not exist
                return new NotFoundResult();
            }

            // Gets the franchise with related movies from the database
            Franchise franchiseToUpdateMovs = await _context.Franchises
                .Include(c => c.Movies)
                .Where(c => c.Id == id)
                .FirstAsync();

            // create a list to store the movies
            List<Movie> movs = new();
            foreach (int charId in movies)
            {
                // find the movie by id
                Movie cert = await _context.Movies.FindAsync(charId);
                if (cert == null)
                    return new BadRequestObjectResult("Certification doesnt exist!");
                movs.Add(cert);
            }

            franchiseToUpdateMovs.Movies = movs;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return new NoContentResult();
        }

        // Gets the Movies with related Franchises from the database 
        public async Task<ActionResult<List<DTOReadMovie>>> GetAllMoviesInFranchise(int franchiseId)
        {
            //Retrieve the franchise using the provided franchiseId
            Franchise franchise = await _context.Franchises
                .Include(c => c.Movies)
                .Where(c => c.Id == franchiseId)
                .FirstAsync();

            //Create a list to store the DTOReadMovie objects
            List<DTOReadMovie> movies = new List<DTOReadMovie>();

            //Iterate through each movie in the franchise
            foreach (Movie movs in franchise.Movies)
            {
                DTOReadMovie MapMovs = mapper.Map<DTOReadMovie>(movs);
                movies.Add(MapMovs);

            }

            return movies;
        }

        public async Task<ActionResult<List<DTOReadCharacter>>> GetAllCharactersInFranchise(int franchiseId)
        {
            //Create a list to store the movies
            List<Movie> movies = new List<Movie>();

            //Create a list to store the characters
            List<DTOReadCharacter> characters = new List<DTOReadCharacter>();

            //Retrieve the franchise using the provided franchiseId
            Franchise franchise = await _context.Franchises
            .Include(f => f.Movies)
            .Where(f => f.Id == franchiseId)
            .FirstOrDefaultAsync();

            //Iterate through each movie in the franchise
            foreach (Movie m in franchise.Movies)
            {
                //Get a list of all characters in the movie using the movieServices
                List<DTOReadCharacter> data = await movieServices.GetAllCharactersInMovie(m.Id);

                //Iterate through each character in the list
                foreach (DTOReadCharacter c in data)
                {
                    //Check if the character is already in the list of characters
                    if (!characters.Contains(c))
                    {
                        //If not, add the character to the list of characters
                        characters.Add(c);
                    }
                }
            }
            return characters;

        }
    }
}
