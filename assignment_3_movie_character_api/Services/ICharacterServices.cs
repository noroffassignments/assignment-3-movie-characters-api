﻿using assignment_3_movie_character_api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace assignment_3_movie_character_api.Services
{
    public interface ICharacterServices
    {
        // Get all characters
        public Task<IEnumerable<Character>> GetAllCharactersAsync();

        // Get specific character by id
        public Task<Character> GetSpecificCharacterAsync(int id);

        // Add a new character
        public Task<Character> AddCharacterAsync(Character character);

        // Update an existing character
        public Task UpdateCharacterAsync(Character character);

        // Delete a character by id
        public Task DeleteCharacterAsync(int id);

        // Check if character exists by id
        public bool CharacterExists(int id);

    }
}
