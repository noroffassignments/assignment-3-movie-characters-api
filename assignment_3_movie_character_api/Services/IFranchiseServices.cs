﻿using assignment_3_movie_character_api.DataTransferObjects;
using assignment_3_movie_character_api.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace assignment_3_movie_character_api.Services
{
    public interface IFranchiseServices
    {
        // Get all franchises
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();

        // Get specific franchise by id
        public Task<Franchise> GetSpecificFranchiseAsync(int id);

        // Add a new franchise
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);

        // Update an existing franchise
        public Task UpdateFranchiseAsync(Franchise franchise);

        // Delete a franchise by id
        public Task DeleteFranchiseAsync(int id);

        // Check if franchise exists by id
        public bool FranchiseExists(int id);

        // Update movie franchise by id
        public Task<IActionResult> UpdateMovieFranchise(int id, List<int> movies);

        // Get all movies in franchise
        public Task<ActionResult<List<DTOReadMovie>>> GetAllMoviesInFranchise(int franchiseId);

        // Get all characters in franchise
        public Task<ActionResult<List<DTOReadCharacter>>> GetAllCharactersInFranchise(int franchiseId);

    }
}
