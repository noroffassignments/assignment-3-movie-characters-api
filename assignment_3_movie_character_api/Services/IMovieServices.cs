﻿using assignment_3_movie_character_api.DataTransferObjects;
using assignment_3_movie_character_api.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace assignment_3_movie_character_api.Services
{
    public interface IMovieServices
    {
        // Retrieves all movies asynchronously
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();

        // Retrieves a specific movie based on its id asynchronously
        public Task<Movie> GetSpecificMovieAsync(int id);

        // Adds a new movie asynchronously
        public Task<Movie> AddMovieAsync(Movie movie);

        // Updates an existing movie asynchronously
        public Task UpdateMovieAsync(Movie movie);

        // Deletes a movie based on its id asynchronously
        public Task DeleteMovieAsync(int id);

        // Checks if a movie exists based on its id
        public bool MovieExists(int id);

        // Updates the characters in a movie
        public Task<IActionResult> UpdateCharacterMovie(int id, List<int> characters);

        // Retrieves all characters in a movie
        public Task<List<DTOReadCharacter>> GetAllCharactersInMovie(int movieId);

    }
}
