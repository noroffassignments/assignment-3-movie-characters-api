﻿using assignment_3_movie_character_api.Data;
using assignment_3_movie_character_api.DataTransferObjects;
using assignment_3_movie_character_api.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_3_movie_character_api.Services
{
    public class MovieServices : IMovieServices
    {
        private readonly CharactersDbContext _context;
        private readonly IMapper mapper;

        public MovieServices(CharactersDbContext context, IMapper mapper)
        {
            // constructor for Dependency Injection of CharactersDbContext and IMapper
            _context = context;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            // Returns a list of all movies from the database using EF Core's ToListAsync method
            return await _context.Movies.Include(m => m.Characters).ToListAsync();
        }

        public async Task<Movie> GetSpecificMovieAsync(int id)
        {
            // Returns a specific movie based on the id provided using EF Core's FirstOrDefaultAsync method
            return await _context.Movies.Include(m => m.Characters).FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task UpdateMovieAsync(Movie movie)
        {
            // Updates the movie in the database by setting its EntityState to Modified and calling SaveChangesAsync method
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            // Adds the movie to the database and calls the SaveChangesAsync method to save it
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        public async Task DeleteMovieAsync(int id)
        {
            // Finds the movie with the id provided, removes it from the database and calls SaveChangesAsync method to save it
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        public bool MovieExists(int id)
        {
            // Returns a boolean indicating if a movie with the provided id exists in the database or not
            return _context.Movies.Any(m => m.Id == id);
        }
        public async Task<IActionResult> UpdateCharacterMovie(int id, List<int> characters)
        {
            // Updates the characters in a movie by id and returns an IActionResult with status codes
            if (!MovieExists(id))
            {
                return new NotFoundResult();
            }

            // Finds the movie with the id provided and includes its characters
            Movie movieToUpdateChars = await _context.Movies
                .Include(c => c.Characters)
                .Where(c => c.Id == id)
                .FirstAsync();

            // Finds all the characters with the ids provided and adds them to the movie
            List<Character> chars = new();
            foreach (int charId in characters)
            {
                Character cert = await _context.Characters.FindAsync(charId);
                if (cert == null)
                    return new BadRequestObjectResult("Certification doesnt exist!");
                chars.Add(cert);
            }

            movieToUpdateChars.Characters = chars;

            try
            {
                // Saves the changes to the database
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return new NoContentResult();
        }

        // This method retrieves a list of characters associated with the specified movie
        public async Task<List<DTOReadCharacter>> GetAllCharactersInMovie(int movieId)
        {
            Movie movie = await _context.Movies
                .Include(c => c.Characters)
                .Where(c => c.Id == movieId)
                .FirstAsync();

            List<DTOReadCharacter> characters = new List<DTOReadCharacter>();
            foreach (Character charctr in movie.Characters)
            {
                DTOReadCharacter MapMovs = mapper.Map<DTOReadCharacter>(charctr);
                characters.Add(MapMovs);

            }

            return characters;
        }
    }
}
