using assignment_3_movie_character_api.Data;
using assignment_3_movie_character_api.DataTransferObjects;
using assignment_3_movie_character_api.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;

namespace assignment_3_movie_character_api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Adding DbContext to the dependency injection container
            services.AddDbContext<CharactersDbContext>(Option => Option.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            // Adding AutoMapper to the dependency injection container
            services.AddAutoMapper(typeof(Startup));

            // Adding data transfer objects for Character operations
            services.AddScoped<DTOReadCharacter>();
            services.AddScoped<DTOCreateCharacter>();
            services.AddScoped<DTOEditCharacter>();
            services.AddTransient<ICharacterServices, CharacterServices>();

            // Adding data transfer objects for Movie operations
            services.AddScoped<DTOReadMovie>();
            services.AddScoped<DTOCreateMovie>();
            services.AddScoped<DTOEditMovie>();
            services.AddTransient<IMovieServices, MovieServices>();

            // Adding data transfer objects for Franchise operations
            services.AddScoped<DTOReadFranchise>();
            services.AddScoped<DTOCreateFranchise>();
            services.AddScoped<DTOEditFranchise>();
            services.AddTransient<IFranchiseServices, FranchiseServices>();

            // Adding the MovieServices to the dependency injection container
            services.AddTransient<MovieServices>();

            // Adding Controllers to the dependency injection container
            services.AddControllers();

            // Adding Swagger to the dependency injection container
            services.AddSwaggerGen(c =>
            {
                // Adding OpenApiInfo to the SwaggerDoc
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Assignment 3 Movie Characters API",
                    Description = "Assignment 3 Movie Characters ASP.NET Core Web API",
                    TermsOfService = new Uri("https://example.com/terms"),
                    Contact = new OpenApiContact
                    {
                        Name = "Niels van Mierlo",
                        Url = new Uri("https://gitlab.com/NVM238"),
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Niek Veenstra - Website",
                        Url = new Uri("https://gitlab.com/NiekVeenstra"),
                    }
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // If the environment is set to Development, show the developer exception page
            // and setup Swagger for API documentation
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "MovieCharactersAPIEFCodeFirst v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}